#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main() {

	srand(ETIME);

	unsigned int fuente[10];
	cout << "Sin ordenar: ";

	for(unsigned int i=0; i<10; i++) {
		fuente[i] = rand() % 10;
		cout << fuente[i] << " ";
	}

	cout << endl;

	bool algoritmo_terminado = true;

	while(algoritmo_terminado == false) {
		for(unsigned int i=0; i<10; i++) {
			
			unsigned int verif = 0;

			if(i != 9 && fuente[i] > fuente[i+1]) {
				unsigned int aux = fuente[i];
				fuente[i] = fuente[i+1];
				fuente[i+1] = aux;
				verif++;
			}

			if(i == 9 && verif == 0)
				algoritmo_terminado = true;
		}
	}

	cout << "Ordenado: ";
	for(int i=0; i<10; i++)
                cout << fuente[i] << " ";

	return 0;
}
